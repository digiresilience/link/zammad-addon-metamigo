# frozen_string_literal: true

class CdrVoiceChannel < ActiveRecord::Migration[5.2]
  def self.up
    Ticket::Article::Type.create_if_not_exists(
      name: 'cdr_voice',
      communication: false,
      updated_by_id: 1,
      created_by_id: 1
    )
    Permission.create_if_not_exists(
      name: 'admin.channel_cdr_voice',
      note: 'Manage %s',
      preferences: {
        translations: ['Channel - Voice']
      }
    )
  end

  def self.down
    t = Ticket::Article::Type.find_by(name: 'cdr_voice')

    t&.destroy

    p = Permission.find_by(name: 'admin.channel_cdr_voice')
    p&.destroy
  end
end
