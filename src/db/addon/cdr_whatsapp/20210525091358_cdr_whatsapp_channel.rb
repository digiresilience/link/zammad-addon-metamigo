# frozen_string_literal: true

class CdrWhatsappChannel < ActiveRecord::Migration[5.2]
  def self.up
    Ticket::Article::Type.create_if_not_exists(
      name: 'cdr_whatsapp',
      communication: true,
      updated_by_id: 1,
      created_by_id: 1
    )
    Permission.create_if_not_exists(
      name: 'admin.channel_cdr_whatsapp',
      note: 'Manage %s',
      preferences: {
        translations: ['Channel - Whatsapp']
      }
    )
  end

  def self.down
    t = Ticket::Article::Type.find_by(name: 'cdr_whatsapp')
    t&.destroy

    p = Permission.find_by(name: 'admin.channel_cdr_whatsapp')
    p&.destroy
  end
end
