# frozen_string_literal: true

class CdrSignalChannel < ActiveRecord::Migration[5.2]
  def self.up
    Ticket::Article::Type.create_if_not_exists(
      name: 'cdr_signal',
      communication: true,
      updated_by_id: 1,
      created_by_id: 1
    )
    Permission.create_if_not_exists(
      name: 'admin.channel_cdr_signal',
      note: 'Manage %s',
      preferences: {
        translations: ['Channel - Signal']
      }
    )
  end

  def self.down
    t = Ticket::Article::Type.find_by(name: 'cdr_signal')
    t&.destroy

    p = Permission.find_by(name: 'admin.channel_cdr_signal')
    p&.destroy
  end
end
