# frozen_string_literal: true

Zammad::Application.routes.draw do
  api_path = Rails.configuration.api_path

  match "#{api_path}/channels_cdr_voice", to: 'channels_cdr_voice#index', via: :get
  match "#{api_path}/channels_cdr_voice", to: 'channels_cdr_voice#add', via: :post
  match "#{api_path}/channels_cdr_voice/:id", to: 'channels_cdr_voice#update', via: :put
  match "#{api_path}/channels_cdr_voice_webhook/:token", to: 'channels_cdr_voice#webhook', via: :post
  match "#{api_path}/channels_cdr_voice_disable", to: 'channels_cdr_voice#disable', via: :post
  match "#{api_path}/channels_cdr_voice_enable", to: 'channels_cdr_voice#enable', via: :post
  match "#{api_path}/channels_cdr_voice", to: 'channels_cdr_voice#destroy', via: :delete
  match "#{api_path}/channels_cdr_voice_rotate_token", to: 'channels_cdr_voice#rotate_token', via: :post
end
