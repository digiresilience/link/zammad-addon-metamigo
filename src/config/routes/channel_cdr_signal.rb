# frozen_string_literal: true

Zammad::Application.routes.draw do
  api_path = Rails.configuration.api_path

  match "#{api_path}/channels_cdr_signal", to: 'channels_cdr_signal#index', via: :get
  match "#{api_path}/channels_cdr_signal", to: 'channels_cdr_signal#add', via: :post
  match "#{api_path}/channels_cdr_signal/:id", to: 'channels_cdr_signal#update', via: :put
  match "#{api_path}/channels_cdr_signal_webhook/:token", to: 'channels_cdr_signal#webhook', via: :post
  match "#{api_path}/channels_cdr_signal_disable", to: 'channels_cdr_signal#disable', via: :post
  match "#{api_path}/channels_cdr_signal_enable", to: 'channels_cdr_signal#enable', via: :post
  match "#{api_path}/channels_cdr_signal", to: 'channels_cdr_signal#destroy', via: :delete
  match "#{api_path}/channels_cdr_signal_rotate_token", to: 'channels_cdr_signal#rotate_token', via: :post
end
