# frozen_string_literal: true

Zammad::Application.routes.draw do
  api_path = Rails.configuration.api_path

  match "#{api_path}/channels_cdr_whatsapp", to: 'channels_cdr_whatsapp#index', via: :get
  match "#{api_path}/channels_cdr_whatsapp", to: 'channels_cdr_whatsapp#add', via: :post
  match "#{api_path}/channels_cdr_whatsapp/:id", to: 'channels_cdr_whatsapp#update', via: :put
  match "#{api_path}/channels_cdr_whatsapp_webhook/:token", to: 'channels_cdr_whatsapp#webhook', via: :post
  match "#{api_path}/channels_cdr_whatsapp_disable", to: 'channels_cdr_whatsapp#disable', via: :post
  match "#{api_path}/channels_cdr_whatsapp_enable", to: 'channels_cdr_whatsapp#enable', via: :post
  match "#{api_path}/channels_cdr_whatsapp", to: 'channels_cdr_whatsapp#destroy', via: :delete
  match "#{api_path}/channels_cdr_whatsapp_rotate_token", to: 'channels_cdr_whatsapp#rotate_token', via: :post
end
