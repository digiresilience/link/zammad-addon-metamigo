# frozen_string_literal: true

Rails.application.config.after_initialize do
  Ticket::Article.add_observer Observer::Ticket::Article::CommunicateCdrSignal.instance

  icon = File.read('public/assets/images/icons/cdr_signal.svg')
  doc = File.open('public/assets/images/icons.svg') { |f| Nokogiri::XML(f) }
  if !doc.at_css('#icon-cdr-signal')
    doc.at('svg').add_child(icon)
    Rails.logger.debug 'signal icon added to icon set'
  else
    Rails.logger.debug 'signal icon already in icon set'
  end
  File.write('public/assets/images/icons.svg', doc.to_xml)
end
