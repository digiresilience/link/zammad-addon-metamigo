# frozen_string_literal: true

class Observer::Ticket::Article::CommunicateCdrWhatsapp < ActiveRecord::Observer
  observe 'ticket::_article'

  def after_create(record)
    # return if we run import mode
    return true if Setting.get('import_mode')

    # if sender is customer, do not communicate
    return true unless record.sender_id

    sender = Ticket::Article::Sender.lookup(id: record.sender_id)
    return true if sender.nil?
    return true if sender.name == 'Customer'

    # only apply on whatsapp messages
    return true unless record.type_id

    type = Ticket::Article::Type.lookup(id: record.type_id)
    return true if type.name !~ /\Acdr_whatsapp/i

    Delayed::Job.enqueue(Observer::Ticket::Article::CommunicateCdrWhatsapp::BackgroundJob.new(record.id))
  end
end
