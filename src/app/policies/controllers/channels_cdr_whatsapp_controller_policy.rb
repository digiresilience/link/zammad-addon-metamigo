# frozen_string_literal: true

module Controllers
  class ChannelsCdrWhatsappControllerPolicy < Controllers::ApplicationControllerPolicy
    default_permit!('admin.channel_cdr_whatsapp')
  end
end
