# frozen_string_literal: true

module Controllers
  class ChannelsCdrVoiceControllerPolicy < Controllers::ApplicationControllerPolicy
    default_permit!('admin.channel_cdr_voice')
  end
end
