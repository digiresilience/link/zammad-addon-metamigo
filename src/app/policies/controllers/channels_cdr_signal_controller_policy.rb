# frozen_string_literal: true

module Controllers
  class ChannelsCdrSignalControllerPolicy < Controllers::ApplicationControllerPolicy
    default_permit!('admin.channel_cdr_signal')
  end
end
