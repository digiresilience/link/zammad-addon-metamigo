class Index extends App.ControllerSubContent
  requiredPermission: 'admin.channel_cdr_signal'
  events:
    'click .js-new':     'new'
    'click .js-edit':    'edit'
    'click .js-delete':  'delete'
    'click .js-disable': 'disable'
    'click .js-enable':  'enable'
    'click .js-rotate-token': 'rotateToken'

  constructor: ->
    super

    #@interval(@load, 60000)
    @load()

  load: =>
    @startLoading()
    @ajax(
      id:   'cdr_signal_index'
      type: 'GET'
      url:  "#{@apiPath}/channels_cdr_signal"
      processData: true
      success: (data) =>
        @stopLoading()
        App.Collection.loadAssets(data.assets)
        @render(data)
    )

  render: (data) =>

    channels = []
    for channel_id in data.channel_ids
      channel = App.Channel.find(channel_id)
      if channel && channel.options
        displayName = '-'
        if channel.group_id
          group = App.Group.find(channel.group_id)
          displayName = group.displayName()
        channel.options.groupName = displayName
      channels.push channel
    @html App.view('cdr_signal/index')(
      channels: channels
    )

  new: (e) =>
    e.preventDefault()
    new FormAdd(
      container: @el.parents('.content')
      load: @load
    )

  edit: (e) =>
    e.preventDefault()
    id = $(e.target).closest('.action').data('id')
    channel = App.Channel.find(id)
    new FormEdit(
      channel: channel
      container: @el.parents('.content')
      load: @load
    )

  delete: (e) =>
    e.preventDefault()
    id   = $(e.target).closest('.action').data('id')
    new App.ControllerConfirm(
      message: 'Sure?'
      callback: =>
        @ajax(
          id:   'cdr_signal_delete'
          type: 'DELETE'
          url:  "#{@apiPath}/channels_cdr_signal"
          data: JSON.stringify(id: id)
          processData: true
          success: =>
            @load()
        )
      container: @el.closest('.content')
    )

  rotateToken: (e) =>
    e.preventDefault()
    id   = $(e.target).closest('.action').data('id')

    new App.ControllerConfirm(
      message: 'This will break the submission form!'
      buttonSubmit: 'Reset token'
      head: 'Reset the submission token?'
      callback: =>
        @ajax(
          id:   'cdr_signal_disable'
          type: 'POST'
          url:  "#{@apiPath}/channels_cdr_signal_rotate_token"
          data: JSON.stringify(id: id)
          processData: true
          success: =>
            @load()
        )
      container: @el.closest('.content')
    )
  disable: (e) =>
    e.preventDefault()
    id   = $(e.target).closest('.action').data('id')
    @ajax(
      id:   'cdr_signal_disable'
      type: 'POST'
      url:  "#{@apiPath}/channels_cdr_signal_disable"
      data: JSON.stringify(id: id)
      processData: true
      success: =>
        @load()
    )

  enable: (e) =>
    e.preventDefault()
    id   = $(e.target).closest('.action').data('id')
    @ajax(
      id:   'cdr_signal_enable'
      type: 'POST'
      url:  "#{@apiPath}/channels_cdr_signal_enable"
      data: JSON.stringify(id: id)
      processData: true
      success: =>
        @load()
    )

class FormAdd extends App.ControllerModal
  head: 'Add Web Form'
  shown: true
  button: 'Add'
  buttonCancel: true
  small: true

  content: ->
    content = $(App.view('cdr_signal/form_add')())
    createOrgSelection = (selected_id) ->
      return App.UiElement.select.render(
        name:       'organization_id'
        multiple:   false
        limit:      100
        null:       false
        relation:   'Organization'
        nulloption: true
        value:      selected_id
        class:      'form-control--small'
      )
    createGroupSelection = (selected_id) ->
      return App.UiElement.select.render(
        name:       'group_id'
        multiple:   false
        limit:      100
        null:       false
        relation:   'Group'
        nulloption: true
        value:       selected_id
        class:      'form-control--small'
      )

    content.find('.js-select').on('click', (e) =>
      @selectAll(e)
    )
    content.find('.js-messagesGroup').replaceWith createGroupSelection(1)
    content.find('.js-organization').replaceWith createOrgSelection(null)
    content

  onClosed: =>
    return if !@isChanged
    @isChanged = false
    @load()

  onSubmit: (e) =>
    @formDisable(e)
    @ajax(
      id:   'cdr_signal_app_verify'
      type: 'POST'
      url:  "#{@apiPath}/channels_cdr_signal"
      data: JSON.stringify(@formParams())
      processData: true
      success: =>
        @isChanged = true
        @close()
      error: (xhr) =>
        data = JSON.parse(xhr.responseText)
        @formEnable(e)
        error_message = App.i18n.translateContent(data.error || 'Unable to save Web Form.')
        @el.find('.alert').removeClass('hidden').text(error_message)
    )

class FormEdit extends App.ControllerModal
  head: 'Web Form Info'
  shown: true
  buttonCancel: true

  content: ->
    content = $(App.view('cdr_signal/form_edit')(channel: @channel))

    createOrgSelection = (selected_id) ->
      return App.UiElement.select.render(
        name:       'organization_id'
        multiple:   false
        limit:      100
        null:       false
        relation:   'Organization'
        nulloption: true
        value:      selected_id
        class:      'form-control--small'
      )
    createGroupSelection = (selected_id) ->
      return App.UiElement.select.render(
        name:       'group_id'
        multiple:   false
        limit:      100
        null:       false
        relation:   'Group'
        nulloption: true
        value:      selected_id
        class:      'form-control--small'
      )

    content.find('.js-messagesGroup').replaceWith createGroupSelection(@channel.group_id)
    content.find('.js-organization').replaceWith createOrgSelection(@channel.options.organization_id)
    content

  onClosed: =>
    return if !@isChanged
    @isChanged = false
    @load()

  onSubmit: (e) =>
    @formDisable(e)
    params = @formParams()
    @channel.options = params
    @ajax(
      id:   'channel_cdr_signal_update'
      type: 'PUT'
      url:  "#{@apiPath}/channels_cdr_signal/#{@channel.id}"
      data: JSON.stringify(@formParams())
      processData: true
      success: =>
        @isChanged = true
        @close()
      error: (xhr) =>
        data = JSON.parse(xhr.responseText)
        @formEnable(e)
        error_message = App.i18n.translateContent(data.error || 'Unable to save changes.')
        @el.find('.alert').removeClass('hidden').text(error_message)
    )

App.Config.set('cdr_signal', { prio: 5100, name: 'Signal', parent: '#channels', target: '#channels/cdr_signal', controller: Index, permission: ['admin.channel_cdr_signal'] }, 'NavBarAdmin')
